package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Grades")
public class Grades {

    private static final String GRADES_SEQUENCE = "address_id_seq";
    private static final String GRADES_GENERATOR = "address_generator";

    @Id
    @SequenceGenerator(name = GRADES_GENERATOR, sequenceName = GRADES_SEQUENCE)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GRADES_GENERATOR)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "value")
    private int value;

    //se specifica relatia inversa(aici e many to one, in studenti one to many
    @ManyToOne
    @JoinColumn(name = "id_students")
    private Students student;

    public Grades() {
    }

    public Grades(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Students getStudent() {
        return student;
    }

    public void setStudent(Students student) {
        this.student = student;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Grades grades = (Grades) o;
        return id == grades.id &&
                value == grades.value &&
                Objects.equals(name, grades.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, value);
    }

    @Override
    public String toString() {
        return "Grades{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
