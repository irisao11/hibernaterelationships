package entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
@NamedQueries({
        @NamedQuery(name = "find_student_by_name",
                    query = "select s from Students s where s.firstName = :firstName"),
        @NamedQuery(name = "update_student",
                    query = "update Students s set s.firstName = :firstName, s.lastName = :lastName, s.email = :email, s.age = :age where s.id = :id"),
        @NamedQuery(name = "delete_student",
                    query = "delete from Students s where s.id = :id")
})

@Entity
@Table(name = "students")

public class Students {

    //cream entitatea

    private static final String STUDENT_SEQUENCE = "student_id_seq";
    private static final String STUDENT_GENERATOR = "student_generator";

    @Id
    //cream un generator pt id, varianta cea mai eficienta pt a incrementa id-ul
    @SequenceGenerator(name = STUDENT_GENERATOR, sequenceName = STUDENT_SEQUENCE)
    //spunem cum sa functioneze. poate fi automat, dar nu e eficient, asa ca tipul de generator este sequence
    //generatorul se incrementeaza din 50 in 50
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = STUDENT_GENERATOR)
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email", unique = true)
    private String email;

    @Column
    private int age;

    //la versiunea aceasta de hibernate merge si fara mappedBy, s-a upgradat
    //cascadarea asigura ca modificarea se propaga asupra tutuoror claselor copil
    //de ex, daca sterg studentul, trebuie sa sterg si adresa
    //prin comenzile de  mai jos fac relatile intre tabele
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "student")
    //join column stabileste foreign key-ul
    private Address address;

    //fetch se fol pt ca iti aduce un obiect. By default e lazy, pt ca aduce un sg obiect din bd.eager iti aduce toata colectia
    //se fol la relatia one to many
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private List<Grades> grades;

    @ManyToMany(cascade = CascadeType.ALL, fetch =FetchType.EAGER, mappedBy = "students")
    List<Courses> courses;

    public Students() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Grades> getGrades() {
        return grades;
    }

    public void setGrades(List<Grades> grades) {
        this.grades = grades;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Courses> getCourses() {
        return courses;
    }

    public void setCourses(List<Courses> courses) {
        this.courses = courses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Students students = (Students) o;
        return id == students.id &&
                age == students.age &&
                Objects.equals(firstName, students.firstName) &&
                Objects.equals(lastName, students.lastName) &&
                Objects.equals(email, students.email) &&
                Objects.equals(address, students.address) &&
                Objects.equals(grades, students.grades);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, email, age, address, grades);
    }

    @Override
    public String toString() {
        return "Students{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", address=" + address +
                ", grades=" + grades +
                ", courses=" + courses +
                '}';
    }
}
