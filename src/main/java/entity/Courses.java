package entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="courses")
public class Courses {

    private static final String COURSES_SEQUENCE = "courses_id_seq";
    private static final String COURSES_GENERATOR = "courses_generator";

    @Id
    @SequenceGenerator(name = COURSES_GENERATOR, sequenceName = COURSES_SEQUENCE)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = COURSES_SEQUENCE)
    private int id;

    @Column(name = "courses_name")
    private String courseName;

    @Column(name = "course_teacher")
    private String teacherName;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "courses_students",
            joinColumns = {@JoinColumn(name = "id_student")},
            inverseJoinColumns = {@JoinColumn(name = "id_course")})
    private List<Students> students;

    public Courses() {
    }

    public Courses(String courseName, String teacherName) {
        this.courseName = courseName;
        this.teacherName = teacherName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public List<Students> getStudents() {
        return students;
    }

    public void setStudents(List<Students> students) {
        this.students = students;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Courses courses = (Courses) o;
        return id == courses.id &&
                Objects.equals(courseName, courses.courseName) &&
                Objects.equals(teacherName, courses.teacherName) &&
                Objects.equals(students, courses.students);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, courseName, teacherName, students);
    }

    @Override
    public String toString() {
        return "Courses{" +
                "id=" + id +
                ", courseName='" + courseName + '\'' +
                ", teacherName='" + teacherName + '\'' +
                '}';
    }
}
