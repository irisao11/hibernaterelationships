package entity;

import javax.persistence.*;

@Entity
@Table(name = "adresa")
public class Address {

    private static final String ADDRESS_SEQUENCE = "address_id_seq";
    private static final String ADDRESS_GENERATOR = "address_generator";

    @Id
    @SequenceGenerator(name = ADDRESS_GENERATOR, sequenceName = ADDRESS_SEQUENCE)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ADDRESS_GENERATOR)
    private int id;

    @Column(name = "Country")
    private String Country;

    @Column(name = "City")
    private String city;

    @Column(name = "Street")
    private String street;

    public Students getStudent() {
        return student;
    }

    public void setStudent(Students student) {
        this.student = student;
    }

    //setam foreign key ul mapped by imi da de unde imi iau datele: se uita in clasa Student
    //se specifica prin ce membru se mapeaza . se duce la campul student, vede ca e mapat dupa address,
    //se uita dupa ce se vrea maparea(student_id) si creaza coloana student_id
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id")
    private Students student;

    public Address() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", Country='" + Country + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                '}';
    }
}
