package db;
//DAO -data acces object - layerul care acceseaza baza de date

import entity.Grades;
import entity.Students;

import javax.persistence.Query;
import java.util.List;

public class StudentDAO extends DBInitializer {

    public void insertSTUDENT(Students student){
        //pt ca studentDAo extinde DBINitializer pot apela toate metodele sale:
        //initializez metoda de deschidere sesiune si tranzactie
        openSessionAndTransaction();
        //fac salvarea inserarii.Persist functioneaza mai bine in hibernate.
        session.persist(student);
        //inchid sesiunea, pt a impinge datele in bd
        closeSessionAndTransaction();
    }
    //insert o lista de studenti
    public void insertStudents(List<Students> students){
        openSessionAndTransaction();
        for(Students student: students){
            try {
                session.persist(student);
            }catch(Exception e){
                System.out.println("Unul din mailuri exista deja in baza de date");
                transaction.rollback();
            }
        }
        closeSessionAndTransaction();
    }

    public Students findStudentById(int id){
        openSessionAndTransaction();
        Students student = session.find(Students.class, id);
        List<Grades> grades = student.getGrades();
        System.out.println(student);
        closeSessionAndTransaction();
        return student;
    }

    public void updateStudent(Students student){
        openSessionAndTransaction();
        session.update(student);
        closeSessionAndTransaction();
    }

    public void deleteStudent(int id){
        openSessionAndTransaction();
        Students student = session.find(Students.class,id );
        session.delete(student);
        closeSessionAndTransaction();
    }

    public Students findStudentByFirstName(String firstName){
        openSessionAndTransaction();
        Query query = session.createNamedQuery("find_student_by_name");
        query.setParameter("firstName", firstName);
        Students student = (Students)query.getSingleResult();
        closeSessionAndTransaction();
        return student;
    }

    public List<Students>  findStudentsByFirstName(String firstName){
        openSessionAndTransaction();
        Query query = session.createNamedQuery("find_student_by_name");
        query.setParameter("firstName", firstName);
        List<Students> students = query.getResultList();
        closeSessionAndTransaction();
        return students;
    }

    public void updateStudentNamedQuery(Students student){
        openSessionAndTransaction();
        Query query = session.createNamedQuery("update_student");
        query.setParameter("firstName", student.getFirstName()).setParameter("lastName", student.getLastName()).setParameter("email", student.getEmail()).setParameter("age", student.getAge()).setParameter("id", student.getId());
        query.executeUpdate();
        closeSessionAndTransaction();
    }

    public void deleteStudentNAmedQuery(int id){
        openSessionAndTransaction();
        Query query = session.createNamedQuery("delete_student");
        query.setParameter("id", id);
        query.executeUpdate();
        closeSessionAndTransaction();
    }
}
