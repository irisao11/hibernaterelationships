package db;

import entity.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Address address = new Address();
        address.setCountry("Romania");
        address.setCity("Iasi");
        address.setStreet("Palat");

        List<Grades> grades = Arrays.asList(
                new Grades("Matematica", 10),
                new Grades(" Informatica", 10),
                new Grades("Fizica", 8)
        );

        List<Courses> courses = Arrays.asList(
                new Courses("Matematica", "Iacob"),
                new Courses("Securitate", "Tipleu"),
                new Courses("Java", "Frasinaru")
        );
        Students student = new Students();
        student.setFirstName("alex");
        student.setLastName("barnaba");
        student.setEmail("a.barnba@yahoo.com");
        student.setAge(35);
        student.setAddress(address);
        address.setStudent(student);
        student.setGrades(grades);
        grades.forEach(x -> x.setStudent(student));

        student.setCourses(courses);
        courses.forEach(x -> x.setStudents(Arrays.asList(student)));
        StudentDAO studentDAO = new StudentDAO();
        //studentDAO.insertSTUDENT(student);
        System.out.println(studentDAO.findStudentById(1));

        //  Students student1 = new Students();
        //  student.setFirstName("irina");
        //  student1.setLastName("moisache");
        //  student1.setEmail("irisao@yahoo.com");
        //  student1.setAge(43);


        // Students student2 = new Students();
        // student.setFirstName("irina");
        //   student2.setLastName("moise");
        //  student2.setEmail("irisao@yahoo.com");
        //  student2.setAge(43);
        //  StudentDAO studentDAO = new StudentDAO();
        //studentDAO.insertStudents(Arrays.asList(student, student1));

        //studentDAO.insertSTUDENT(student);
        //landa expression din java 8
        //studentDAO.findStudentsByFirstName("alex").forEach(System.out::println);
        //    System.out.println(studentDAO.findStudentById(52));
        //student.setId(52);
        //student.setFirstName("Gogu");
        // studentDAO.updateStudent(student);
        //studentDAO.deleteStudent(52);
        //System.out.println(studentDAO.findStudentByFirstName("alex"));
        //studentDAO.updateStudentNamedQuery(student);
        //studentDAO.deleteStudent(202);
        // studentDAO.deleteStudentNAmedQuery(453);
        //List<Persoana> persoane = Arrays.asList(
        // new Persoana("Ion", "Ion", 42),
        // new Persoana("Mihai", "ceva", 22),
        //  new Persoana("Maria", "Ioana", 55));
        //persoane.forEach(System.out::println);
        //  Collections.sort(persoane);
        // System.out.println("sorted:");
        //  persoane.forEach(System.out::println);

        // List<Persoana1> persoane1 = Arrays.asList(
        //         new Persoana1("Ion", "Ion", 42),
        //         new Persoana1("Mihai", "ceva", 22),
        //          new Persoana1("Maria", "Ioana", 55));
        //  persoane1.forEach(System.out::println);
        //  Collections.sort(persoane1, Comparator.comparingInt(Persoana1::getVarsta));
        //  System.out.println("sorted:");
        //   persoane1.forEach(System.out::println);


        //AddressDAO addressDAO = new AddressDAO();
        //addressDAO.insertAddress(address);
    }
}
