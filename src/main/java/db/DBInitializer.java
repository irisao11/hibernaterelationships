package db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public abstract class DBInitializer {
    //sunt protected pt ca vor trebui folosite de alte clase
    private SessionFactory sessionFactory;
    protected Session session;
    protected Transaction transaction;

    //constructorul - se gaseste pe net hibernate session factory
    //am pus sessionfactory in constructor pt ca atunci cand clasa dbInitializer este extinsa,
    //clasa copil sa construiasca automat un sessionFactory
    //pt ca inainte sa ai un copil, trebuie sa avem un parinte
    //fara sessionFactory nu putem sa avem o sesiune si sa facem operatii
    public DBInitializer(){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        MetadataSources sources = new MetadataSources(registry);
        Metadata metadata = sources.getMetadataBuilder().build();
        sessionFactory = metadata.getSessionFactoryBuilder().build();
    }

    //avem tot aici tranzactia si sesiunea
    //facem o metoda de deschidere sesiune si tranzactie

    protected void openSessionAndTransaction(){
        session = sessionFactory.openSession();
        transaction = session.beginTransaction();
    }

    //facem metoda de inchidere sesiune si tranzactie

    protected void closeSessionAndTransaction(){
        transaction.commit();
        session.close();
    }
}
