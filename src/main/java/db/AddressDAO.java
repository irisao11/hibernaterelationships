package db;

import entity.Address;

public class AddressDAO extends DBInitializer {

    public void insertAddress(Address address){
        openSessionAndTransaction();
        session.persist(address);
        closeSessionAndTransaction();
    }


}
